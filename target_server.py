import socket
import threading
import thread
clients = set()
clients_lock = threading.Lock()

def on_new_client(clientsocket,addr):
    while True:
        msg = clientsocket.recv(1024)
        print (addr, ' >> ', msg)
        clientsocket.send(msg)
    clientsocket.close()
def listener(client, address):
    print ("Accepted connection from: ", address)
    with clients_lock:
        clients.add(client)
    try:
        while True:
            data = client.recv(1024)
            if not data:
                break
            else:
                print (repr(data))
                with clients_lock:
                    for c in clients:
                        c.sendall(data)
    finally:
        with clients_lock:
            clients.remove(client)
            client.close()


s = socket.socket()
host = '0.0.0.0'
port = 8765

print ('Target Server started!')
print ('Waiting for clients...')
s.bind((host, port))
s.listen(5)

if __name__ == '__main__':
    while True:
       c, addr = s.accept()
       #thread.start_new_thread(on_new_client,(c,addr))
       thread.start_new_thread(listener,(c,addr))
    s.close()
